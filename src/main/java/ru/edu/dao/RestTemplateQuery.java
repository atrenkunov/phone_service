package ru.edu.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.edu.dto.event.EventDTO;
import ru.edu.dto.user.User;
import ru.edu.service.TelegramBot;

import java.util.Arrays;
import java.util.List;

@Component
public class RestTemplateQuery {

    private final Logger logger = LogManager.getLogger(TelegramBot.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${client.event-service}")
    private String eventService;

    @Value("${client.auth-service}")
    private String authService;

    public boolean deleteEvent(String eventId) throws RestClientException {
        final String additionalUrl = "/event/" + eventId;
        String url = eventService + additionalUrl;

        int statusCode = restTemplate.exchange(url, HttpMethod.DELETE, null,
                EventDTO.class, eventId).getStatusCodeValue();

        return statusCode == HttpStatus.OK.value();
    }

    public boolean cancelEvent(String eventId) throws RestClientException {
        final String additionalUrl = "/event/" + eventId + "/cancel";
        String url = eventService + additionalUrl;

        int statusCode = restTemplate.exchange(url, HttpMethod.PUT, null,
                EventDTO.class).getStatusCodeValue();

        return statusCode == HttpStatus.OK.value();
    }

    public List<EventDTO> getAllUserEvents(String userId) throws RestClientException {
        final String additionalUrl = "/event/all/byUserId?userId={userId}";
        String url = eventService + additionalUrl;

        return Arrays.asList(restTemplate.getForObject(url, EventDTO[].class, userId));
    }

    public List<EventDTO> getUserEventsByDate(String userId, String date) throws RestClientException {
        final String additionalUrl = "/event/all/byDate?userId={userId}&date={date}";
        String url = eventService + additionalUrl;

        return Arrays.asList(restTemplate.getForObject(url, EventDTO[].class, userId, date));
    }

    public User getUserByTelegramChat(String chatId) {
        final String additionalUrl = "/auth/user/byTelegramChat?chatId={chatId}";
        String url = authService + additionalUrl;

        try {
            return restTemplate.getForObject(url, User.class, chatId);
        } catch (HttpServerErrorException exception) {
            logger.error(exception);
            return null;
        }
    }
}
