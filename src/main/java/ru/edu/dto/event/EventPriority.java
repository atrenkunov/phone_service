package ru.edu.dto.event;

/**
 * Event priority.
 */
public enum EventPriority {
    HIGH,
    MEDIUM,
    LOW
}
