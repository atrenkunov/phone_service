package ru.edu.dto.event;

/**
 * Event state.
 */
public enum EventState {
    TODO,
    CANCELED,
    DONE
}