package ru.edu.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.edu.service.TelegramUtils;
import ru.reboot.dto.EventDTO;
import ru.edu.service.TelegramBot;
import ru.reboot.dto.User;

@Component
public class EventNotificationListener {

    @Value("${client.auth-service}")
    private String authService;

    private static String urlGetUserById = "/auth/user/byUserId?userId=%s";

    private final Logger logger = LoggerFactory.getLogger(EventNotificationListener.class);

    private RestTemplate restTemplate;
    private TelegramBot telegramBot;

    @KafkaListener(topics = "EVENT_NOTIFICATION", groupId = "group_id")
    public void onEventNotification(EventDTO eventDTO) {
        logger.info("#### <<< Consumed message <<< {}", eventDTO);
        String userId = eventDTO.getUserId();
        String url = String.format(authService + urlGetUserById, userId);
        try{
            User user = restTemplate.getForObject(url, User.class);
            if (user.getTelegramChatId() != null
            && !user.getTelegramChatId().equalsIgnoreCase("null")
            && !user.getTelegramChatId().equalsIgnoreCase("")) {
                String message = user.getFirstName() + ", напоминаю о предстоящем событии: " + eventDTO.getName() + " в " + eventDTO.getTime().toString();
                SendMessage sendMessage = TelegramUtils.createMessage(user.getTelegramChatId(), message);
                telegramBot.execute(sendMessage);
            }
        } catch (Exception ex){
            logger.error("Error on rest to AuthService.getUserById");
        }
    }

    @Autowired
    public void setTelegramBot(TelegramBot telegramBot) {
        this.telegramBot = telegramBot;
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
