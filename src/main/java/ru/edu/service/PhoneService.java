package ru.edu.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Component
public class PhoneService {

    private final Map<String, String> mapCodeUserId = new HashMap<>();

    private static final Logger logger = LogManager.getLogger(PhoneService.class);

    public String getUserIdByTelegramCode(String code) {
        return mapCodeUserId.get(code);
    }

    public String generateTelegramCode(String userId) {
        logger.info("Method PhoneService.generateTelegramCode invoked");
        String code = generateCode();
        mapCodeUserId.put(code, userId);
        logger.info("Method generateTelegramCode: " + code);
        return code;
    }

    private String generateCode() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 4;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }
}
