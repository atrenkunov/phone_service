package ru.edu.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.edu.service.command.*;

import javax.annotation.PostConstruct;

import static ru.edu.service.command.constants.Constants.HELP_MESSAGE;

/**
 * Telegram bot.
 */
public class TelegramBot extends TelegramLongPollingCommandBot {

    private final Logger logger = LogManager.getLogger(TelegramBot.class);

    private final String botUserName;
    private final String botToken;

    @Autowired
    private MyHiCommand myHiCommand;

    @Autowired
    private CancelCommand cancelCommand;

    @Autowired
    private TaskDateCommand taskDateCommand;

    @Autowired
    private MyHelpCommand myHelpCommand;

    @Autowired
    private FinishCommand finishCommand;

    @Autowired
    private AttachChatCommand attachChatCommand;

    @Autowired
    private CreateEventCommand createEventCommand;


    public TelegramBot(String botUserName, String botToken) {
        this.botUserName = botUserName;
        this.botToken = botToken;
    }

    @PostConstruct
    public void init() {
        register(myHiCommand);
        register(cancelCommand);
        register(taskDateCommand);
        register(myHelpCommand);
        register(attachChatCommand);
        register(finishCommand);
        register(createEventCommand);
    }

    @Override
    public String getBotUsername() {
        return botUserName;
    }

    @Override
    public void processNonCommandUpdate(Update update) {
        String chatId = String.valueOf(update.getMessage().getChatId());
        String answer = getNonCommandAnswer(update);

        SendMessage message = TelegramUtils.createMessage(chatId, answer);
        try {
            execute(message);
        } catch (Exception ex) {
            logger.error("Failed to .processNonCommandUpdate error={}", ex.toString(), ex);
        }
    }

    private String getNonCommandAnswer(Update update) {
        String firstLine = "Я не знаю такой команды: " + update.getMessage().getText();
        StringBuilder builder = new StringBuilder();

        builder.append(firstLine);
        builder.append("\n");
        builder.append(HELP_MESSAGE);

        return builder.toString();
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
