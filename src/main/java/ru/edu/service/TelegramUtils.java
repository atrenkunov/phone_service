package ru.edu.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public class TelegramUtils {

    public static SendMessage createMessage(String chatId, String text) {

        SendMessage message = new SendMessage();

        message.enableMarkdown(true);
        message.setChatId(chatId);
        message.setText(text);

        return message;
    }

    public static SendMessage createHtmlMessage(String chatId, String htmlMessage) {

        SendMessage message = new SendMessage();

        message.enableHtml(true);
        message.setParseMode("html");
        message.setChatId(chatId);
        message.setText(htmlMessage);

        return message;
    }
}
