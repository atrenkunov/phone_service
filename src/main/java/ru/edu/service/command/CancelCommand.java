package ru.edu.service.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import ru.edu.dto.event.EventDTO;
import ru.edu.dao.RestTemplateQuery;
import ru.edu.dto.event.EventState;
import ru.edu.service.TelegramBot;
import ru.edu.service.TelegramUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static ru.edu.service.command.constants.Constants.DATE_PATTERN;
import static ru.edu.service.command.constants.Constants.NO_TELEGRAM_ACCOUNT_MESSAGE;

@Component
public class CancelCommand extends BotCommand {

    private final Logger logger = LogManager.getLogger(TelegramBot.class);
    private final int[] PARAMETER_COUNT = {1, 2};
    private final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);
    private final String PARAMETER_LAYOUT = "\\d+";
    private final String WRONG_INPUT_MESSAGE = String.format("Неправильный ввод.\n" +
            "Используйте команду: /cancel НОМЕР ДАТА (%s) для отмены события.", DATE_PATTERN);
    private final String WRONG_EVENT_NUMBER_MESSAGE = "События с номером %d за %s не существует.";
    private final String SUCCESS_DELETING = "Событие \"%s\" отменено.";
    private final String FAILED_DELETING = "События \"%s\" не удалось отменить.";
    private final Predicate<EventDTO> statusFilter = e -> true;

    @Autowired
    private RestTemplateQuery restTemplateQuery;

    private String finalMessage;
    private String targetDateString;
    private int eventNumber;
    private List<EventDTO> eventList;
    private EventDTO targetEvent;
    private User user;
    private String userId;
    private String dbUserId;
    private boolean deleteSucceeded;

    public CancelCommand(String commandIdentifier, String description) {
        this();
    }

    public CancelCommand() {
        super("cancel", "Процессор для отмены команды по номеру за дату");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        logger.info("Method .execute command {} started", this.getCommandIdentifier());

        try {
            this.user = user;
            prepareFinalMessage(strings);

            SendMessage message = TelegramUtils.createMessage(String.valueOf(user.getId()), finalMessage);
            absSender.execute(message);
        } catch (Exception ex) {
            logger.error("Failed to .execute command={} error={}", getCommandIdentifier(), ex.toString(), ex);
        }

        logger.info("Method .execute command {} finished", this.getCommandIdentifier());
    }

    private void prepareFinalMessage(String[] strings) {
        if (isInputCorrect(strings)) {
            this.userId = user.getId().toString();

            dbUserId = getBdUserId();
            if (isDbUserIdCorrect()) {
                eventList = restTemplateQuery.getUserEventsByDate(dbUserId, targetDateString)
                        .stream()
                        .filter(statusFilter)
                        .collect(Collectors.toList());

                if (isEventNumberCorrect()) {
                    targetEvent = getTargetEvent();
                    deleteSucceeded = restTemplateQuery.cancelEvent(targetEvent.getId());
                    setMessageAfterDelete();
                }
            }
        }
    }

    private void setMessageAfterDelete() {
        String layout = deleteSucceeded ? SUCCESS_DELETING : FAILED_DELETING;
        String eventName = targetEvent.getName();

        finalMessage = String.format(layout, eventName);
        logger.info(finalMessage);
    }


    private EventDTO getTargetEvent() {
        int eventNumberForList = eventNumber - 1;
        return eventList.get(eventNumberForList);
    }

    private boolean isEventNumberCorrect() {
        if (eventList.size() >= eventNumber) {
            return true;
        }

        finalMessage = String.format(WRONG_EVENT_NUMBER_MESSAGE, eventNumber, targetDateString);
        logger.info(finalMessage);
        return false;
    }


    private String getBdUserId() {
        ru.edu.dto.user.User dbUser = restTemplateQuery.getUserByTelegramChat(userId);
        return dbUser == null ? null : dbUser.getUserId();
    }

    private boolean isDbUserIdCorrect() {
        if (1 == 1) {
            return true;
        }

        if (dbUserId != null) {
            return true;
        }

        this.finalMessage = NO_TELEGRAM_ACCOUNT_MESSAGE;
        logger.info(finalMessage);
        return false;
    }

    private boolean isInputCorrect(String[] strings) {
        if (checkInput(strings)
                && defineNumberIfPossible(strings)
                && defineDateIfPossible(strings)) {
            return true;
        }

        this.finalMessage = WRONG_INPUT_MESSAGE;
        logger.info(finalMessage + "\n" + Arrays.asList(strings));
        return false;
    }

    private boolean checkInput(String[] strings) {
        int len = strings.length;
        return IntStream.of(PARAMETER_COUNT)
                .anyMatch(i -> i == len);
    }

    private boolean defineNumberIfPossible(String[] strings) {
        String inputNumber = strings[0];

        if (!inputNumber.matches(PARAMETER_LAYOUT)) {
            return false;
        }

        eventNumber = Integer.parseInt(inputNumber);
        return true;
    }

    private boolean defineDateIfPossible(String[] strings) {
        int len = strings.length;

        if (len == 1) {
            targetDateString = LocalDate.now().format(DATE_FORMATTER);
            return true;
        }

        if (len == 2) {
            return isInputDateCorrect(strings);
        }

        logger.error("Failed to .execute command={} cause of wrong count of parameters {}", getCommandIdentifier(), len);
        return false;
    }

    private boolean isInputDateCorrect(String[] strings) {
        try {
            targetDateString = strings[1];
            LocalDate.parse(targetDateString, DATE_FORMATTER);
            return true;
        } catch (DateTimeParseException exception) {
            finalMessage = WRONG_INPUT_MESSAGE;
            logger.info(finalMessage);
            return false;
        }
    }
}
