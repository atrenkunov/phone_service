package ru.edu.service.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.edu.dto.event.EventDTO;
import ru.edu.dto.event.EventPriority;
import ru.edu.dto.event.EventState;
import ru.edu.service.TelegramUtils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class CreateEventCommand extends BotCommand {
    private static final Logger logger = LogManager.getLogger(CreateEventCommand.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${client.auth-service}")
    private String authService;

    @Value("${client.event-service}")
    private String EVENT_SERVICE_URL;

    public CreateEventCommand() {
        super("add", "Процессор для команды add");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        logger.info("Пользователь {}. Начато выполнение команды {}", user,
                this.getCommandIdentifier());

        if (strings.length == 0) {
            SendMessage messageEmpty = TelegramUtils.createMessage(String.valueOf(user.getId()), "Используйте данный формат для создания события: \n/add ИмяСобытия ЧЧ:ММ (Если событие сегодня) \n/add ИмяСобытия ГГГГ-ММ-ДД ЧЧ:ММ (Время по желанию)");
            try {
                absSender.execute(messageEmpty);
            } catch (TelegramApiException e) {
                logger.error("Failed to send .messageEmpty command={} error={}", getCommandIdentifier(), e.toString(), e);
                throw new RuntimeException();
            }
        } else {
            String firstString = strings[0];
            String userId = getUserId(String.valueOf(chat.getId()));
            if(userId == null){
                logger.error("User not found");
                SendMessage messageUserNotFound = TelegramUtils.createMessage(String.valueOf(user.getId()), "Привяжите Ваш telegram-аккаунт в настройках SDiary");
                try {
                    absSender.execute(messageUserNotFound);
                } catch (TelegramApiException e) {
                    logger.error("Failed to send .messageUserNotFound command={} error={}", getCommandIdentifier(), e.toString(), e);
                }
            }
            EventDTO eventDTO = createEvent(strings, userId, absSender, String.valueOf(user.getId()));
            if (eventDTO == null){
                logger.error("Event not create, Illegal argument");
                SendMessage messageEventNotCreate = TelegramUtils.createMessage(String.valueOf(user.getId()), "Неверный формат. Отправьте -> /add для получения информации о шаблоне.");
                try {
                    absSender.execute(messageEventNotCreate);
                } catch (TelegramApiException e){
                    logger.error("Filed create new event command={} error={}", getCommandIdentifier(), e.toString(), e);
                }
            } else {
                try {
                    String url = EVENT_SERVICE_URL + "/event";
                    EventDTO event = restTemplate.postForObject(url,  eventDTO, EventDTO.class);
                    SendMessage message = TelegramUtils.createMessage(String.valueOf(user.getId()), "Событие " + event.getName() + " успешно создано!");
                    absSender.execute(message);
                } catch (Exception e){
                    logger.error("Error rest EventService.createEvent from telegram");
                }
            }
        }
    }

    /**
     * Создание события из полученного сообщения от пользователя
     * @param message
     * @return EventDTO
     */
    private EventDTO createEvent (String[] message, String userId, AbsSender absSender, String chatId){
        EventDTO eventDTO = new EventDTO();
        eventDTO.setUserId(userId);
        StringBuilder eventName = new StringBuilder();


        for(String s: message) {
            Pattern patternDate = Pattern.compile("\\b[0-9]\\d\\d\\d\\-\\d\\d\\-\\d\\d$");
            Matcher matcherDate = patternDate.matcher(s);
            if (matcherDate.find()) {
                try {
                    eventDTO.setDate(LocalDate.parse(s.substring(matcherDate.start(), matcherDate.end())));
                } catch (Exception e){
                    logger.error("Ошибка при парсинге даты", e);
                    return null;
                }
            }

            Pattern patternTime = Pattern.compile("\\b[0-9]\\d\\:\\d\\d$");
            Matcher matcherTime = patternTime.matcher(s);
            if (matcherTime.find()) {
                try {
                    eventDTO.setTime(LocalTime.parse(s.substring(matcherTime.start(), matcherTime.end())));
                } catch (Exception e){
                    logger.error("Ошибка при парсинге времени", e);
                    return null;
                }
            }
        }
        if(eventDTO.getDate() == null){
            eventDTO.setDate(LocalDate.now());
        }

        for(String s: message){
            if(s.equals(eventDTO.getDate().toString())){

            } else if(eventDTO.getTime() == null){
                eventName.append(s+" ");
            } else {
                if (s.equals(eventDTO.getTime().toString())){

                } else {
                    eventName.append(s+" ");
                }
            }
        }

        if (eventName.toString().isEmpty()){
            return null;
        } else {
            eventDTO.setName(eventName.toString());
            eventDTO.setState(EventState.TODO);
            eventDTO.setPriority(EventPriority.HIGH);

            return eventDTO;
        }
    }

    /**
     * Получить userId из БД по chatId Telegram
     *
     * @param chatId id Telegram чата пользователя
     * @return userId
     */
    private String getUserId(String chatId) {
        logger.info("Method .getUserId started chatId={}", chatId);
        final String additionalUrl = "/auth/user/byTelegramChat?chatId=" + chatId;
        String url = authService + additionalUrl;
        ru.edu.dto.user.User user;
        try {
            user = restTemplate.getForObject(url, ru.edu.dto.user.User.class, chatId);
            return user == null ? null : user.getUserId();
        } catch (HttpServerErrorException exception) {
            logger.error("Method .getUserId failed chatId={}", chatId);
            return null;
        }
    }
}
