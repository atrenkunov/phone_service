package ru.edu.service.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.edu.dto.event.EventDTO;
import ru.edu.service.TelegramUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class FinishCommand extends BotCommand {
    private static final Logger logger = LogManager.getLogger(FinishCommand.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${client.auth-service}")
    private String authService;

    @Value("${client.event-service}")
    private String EVENT_SERVICE_URL;

    public FinishCommand() {
        super("finish", "Процессор для команды finish");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        logger.info("Пользователь %s. Начато выполнение команды %s", user,
                this.getCommandIdentifier());

        if (strings.length == 0) {
            SendMessage messageEmpty = TelegramUtils.createMessage(String.valueOf(user.getId()), "Укажите номер задачи после команды\n/finish");
            try {
                absSender.execute(messageEmpty);
            } catch (TelegramApiException e) {
                logger.error("Failed to send .messageEmpty command={} error={}", getCommandIdentifier(), e.toString(), e);
                throw new RuntimeException();
            }
        } else {
            String firstString = strings[0];
            String userId = getUserId(String.valueOf(chat.getId()));
            String eventId = getEventId(userId, firstString);
            if (eventId == null) {
                SendMessage messageNotNumber = TelegramUtils.createMessage(String.valueOf(user.getId()), "Неверно указан номер задачи");
                try {
                    absSender.execute(messageNotNumber);
                } catch (TelegramApiException e) {
                    logger.error("Failed to send .messageNotNumber command={} error={}", getCommandIdentifier(), e.toString(), e);
                }
            }
            boolean result = finishEvent(eventId);
            SendMessage messageSuccess = TelegramUtils.createMessage(String.valueOf(user.getId()), "Задача №" + firstString + " выполнена");
            SendMessage messageFail = TelegramUtils.createMessage(String.valueOf(user.getId()), "Задача " + firstString + " не найдена");
            try {
                if (result)
                    absSender.execute(messageSuccess);
                else
                    absSender.execute(messageFail);
            } catch (Exception ex) {
                logger.error("Failed to send .messageSuccess or .messageFail command={} error={}", getCommandIdentifier(), ex.toString(), ex);
            }
        }
    }

    /**
     * Получить userId из БД по chatId Telegram
     *
     * @param chatId id Telegram чата пользователя
     * @return userId
     */
    private String getUserId(String chatId) {
        logger.info("Method .getUserId started chatId={}", chatId);
        final String additionalUrl = "/auth/user/byTelegramChat?chatId=" + chatId;
        String url = authService + additionalUrl;
        ru.edu.dto.user.User user;
        try {
            user = restTemplate.getForObject(url, ru.edu.dto.user.User.class, chatId);
            return user == null ? null : user.getUserId();
        } catch (HttpServerErrorException exception) {
            logger.error("Method .getUserId failed chatId={}", chatId);
            return null;
        }
    }

    /**
     * Получить eventId из БД по userId
     *
     * @param userId id пользователя в БД
     * @param firstString номер задачи переданный пользователем
     * @return id залачи
     */
    private String getEventId(String userId, String firstString) {
        logger.info("Method .getEventId started userId={} firstString ={}", userId, firstString);
        final String serviceUrl = ("/event/all/byUserId?userId=" + userId);
        final String targetUrl = EVENT_SERVICE_URL + serviceUrl;
        int id;
        try {
            id = Integer.parseInt(firstString);
        } catch (NumberFormatException e) {
            logger.error("Method .getEventId failed userId={} firstString={}", userId, firstString);
            return null;
        }
        try {
            List<EventDTO> userEvents = Arrays.asList(restTemplate.getForObject(targetUrl, EventDTO[].class));
            List<EventDTO> newList = new ArrayList<>();
            String date = LocalDate.now().toString();
            for (EventDTO e : userEvents) {
                if (date.equals(e.getDate().toString())) {
                    newList.add(e);
                }
            }
            EventDTO eventDTO = newList.get(id-1);
            String foundEventId = eventDTO.getId();
            return foundEventId;
        } catch (Exception e) {
            logger.error("Method .getAllUserEvents failed", e);
        }
        return null;
    }

    /**
     * Поменять статус задачи в БД на DONE
     *
     * @param eventId id задачи
     * @return true or false
     */
    private boolean finishEvent(String eventId) {
        logger.info("Method .finishEvent started eventId={}", eventId);
        final String serviceUrl = "/event/" + eventId + "/finish";
        final String targetUrl = EVENT_SERVICE_URL + serviceUrl;
        try {
            restTemplate.put(targetUrl, null);
            logger.info("Method .finishEvent completed eventId={}" , eventId);
            return true;
        } catch (Exception e) {
            logger.error("Method .finishEvent failed eventId={}" , eventId);
            return false;
        }
    }
}