package ru.edu.service.command;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import ru.edu.service.TelegramUtils;

import static ru.edu.service.command.constants.Constants.HELP_MESSAGE;

@Component
public class MyHelpCommand extends BotCommand {

    private static final Logger logger = LogManager.getLogger(MyHelpCommand.class);

    @Autowired
    private RestTemplate restTemplate;

    public MyHelpCommand() {
        super("help", "Процессор для команды help");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        logger.debug(String.format("Пользователь %s. Начато выполнение команды %s", user,
                this.getCommandIdentifier()));

        SendMessage message = TelegramUtils.createMessage(String.valueOf(user.getId()), HELP_MESSAGE);

        logger.debug(String.format("Пользователь %s. Завершено выполнение команды %s", user,
                this.getCommandIdentifier()));

        try {
            absSender.execute(message);
        } catch (Exception ex) {
            logger.error("Failed to .execute command={} error={}", getCommandIdentifier(), ex.toString(), ex);
        }
    }
}
