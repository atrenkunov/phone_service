package ru.edu.service.command;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import ru.edu.service.TelegramUtils;

@Component
public class MyHiCommand extends BotCommand {

    private static final Logger logger = LogManager.getLogger(MyHiCommand.class);

    @Autowired
    private RestTemplate restTemplate;

    public MyHiCommand() {
        super("hi", "Процессор для команды hi");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        SendMessage message = TelegramUtils.createMessage(String.valueOf(user.getId()), "Приветик! Как дела?");
        try {
            absSender.execute(message);
        } catch (Exception ex) {
            logger.error("Failed to .execute command={} error={}", getCommandIdentifier(), ex.toString(), ex);
        }
    }
}
