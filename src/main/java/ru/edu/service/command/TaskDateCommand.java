package ru.edu.service.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import ru.edu.dao.RestTemplateQuery;
import ru.edu.dto.event.EventDTO;
import ru.edu.dto.event.EventState;
import ru.edu.service.TelegramBot;
import ru.edu.service.TelegramUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static ru.edu.dto.event.EventState.DONE;
import static ru.edu.dto.event.EventState.TODO;
import static ru.edu.dto.event.EventState.CANCELED;
import static ru.edu.service.command.constants.Constants.DATE_PATTERN;
import static ru.edu.service.command.constants.Constants.NO_TELEGRAM_ACCOUNT_MESSAGE;

@Component
public class TaskDateCommand extends BotCommand {

    private final Logger logger = LogManager.getLogger(TelegramBot.class);
    private final int[] PARAMETER_COUNT = {0, 1};
    private final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);
    private final String WRONG_INPUT_MESSAGE = String.format("Неправильный ввод.\n" +
            "Используйте команду: /task ДАТА (%s) для получения событий за дату.", DATE_PATTERN);
    private final String NO_EVENTS_MESSAGE = "На данный момент у вас нет событий за %s";
    private final Predicate<EventDTO> statusFilter = e -> true;
    private final Map<EventState, String> statusMap = new HashMap<>();


    @Autowired
    private RestTemplateQuery restTemplateQuery;

    private String finalMessage;
    private String targetDateString;
    private List<EventDTO> eventList;
    private User user;
    private String userId;
    private String dbUserId;

    {
        statusMap.put(TODO, "⏱");
        statusMap.put(DONE, "✅");
        statusMap.put(CANCELED, "❌");
    }

    public TaskDateCommand(String commandIdentifier, String description) {
        this();
    }

    public TaskDateCommand() {
        super("task", "Процессор для получения списка событий за дату");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        logger.info("Method .execute command {} started", this.getCommandIdentifier());

        try {
            this.user = user;
            prepareFinalMessage(strings);

            SendMessage message = TelegramUtils.createMessage(String.valueOf(user.getId()), finalMessage);
            absSender.execute(message);
        } catch (Exception ex) {
            logger.error("Failed to .execute command={} error={}", getCommandIdentifier(), ex.toString(), ex);
        }

        logger.info("Method .execute command {} finished", this.getCommandIdentifier());
    }

    private void prepareFinalMessage(String[] strings) {
        if (isInputCorrect(strings)) {
            this.userId = user.getId().toString();
            this.dbUserId = getBdUserId();

            if (isDbUserIdCorrect()) {
                eventList = restTemplateQuery.getUserEventsByDate(dbUserId, targetDateString);
                finalMessage = getFinalMessageFromEventList();
            }
        }
    }

    private String getFinalMessageFromEventList() {
        if (eventList.isEmpty()) {
            return String.format(NO_EVENTS_MESSAGE, targetDateString);
        } else {
            return buildFinalMessageFromEventList();
        }
    }

    private String buildFinalMessageFromEventList() {
        String firstLine = "Задачи на %s:";
        firstLine = String.format(firstLine, targetDateString);

        StringBuilder builder = new StringBuilder(firstLine);
        int index = 0;

        for (int i = 0; i < eventList.size(); i++) {
            EventDTO event = eventList.get(i);

            if (!statusFilter.test(event)) {
                continue;
            }
            index++;

            String line = getLine(event, index);
            builder.append(line);
        }

        return builder.toString();
    }

    private String getLine(EventDTO event, int index) {
        final int totalLen = 30;
        final String layout = "\n%d. %s";
        String line = String.format(layout, index, event.getName());

        int remainedLen = totalLen - line.length();

        EventState eventState = event.getState();
        String status = eventState.toString() + statusMap.get(eventState);

        line += ("%" + remainedLen + "s");
        return String.format(line, status);
    }

    private String getBdUserId() {
        ru.edu.dto.user.User dbUser = restTemplateQuery.getUserByTelegramChat(userId);
        return dbUser == null ? null : dbUser.getUserId();
    }

    private boolean isDbUserIdCorrect() {
        if (dbUserId != null) {
            return true;
        }

        finalMessage = NO_TELEGRAM_ACCOUNT_MESSAGE;
        logger.info(finalMessage);
        return false;
    }

    private boolean isInputCorrect(String[] strings) {
        if (checkInput(strings) && defineDateIfPossible(strings)) {
            return true;
        }

        this.finalMessage = WRONG_INPUT_MESSAGE;
        logger.info(finalMessage + "\n" + Arrays.asList(strings));
        return false;
    }

    private boolean checkInput(String[] strings) {
        int len = strings.length;
        return IntStream.of(PARAMETER_COUNT)
                .anyMatch(i -> i == len);
    }

    private boolean defineDateIfPossible(String[] strings) {
        int len = strings.length;

        if (len == 0) {
            targetDateString = LocalDate.now().format(DATE_FORMATTER);
            return true;
        }

        if (len == 1) {
            return isInputDateCorrect(strings);
        }

        logger.error("Failed to .execute command={} cause of wrong count of parameters {}", getCommandIdentifier(), len);
        return false;
    }

    private boolean isInputDateCorrect(String[] strings) {
        try {
            targetDateString = strings[0];
            LocalDate.parse(targetDateString, DATE_FORMATTER);
            return true;
        } catch (DateTimeParseException exception) {
            finalMessage = WRONG_INPUT_MESSAGE;
            logger.info(finalMessage);
            return false;
        }
    }
}
