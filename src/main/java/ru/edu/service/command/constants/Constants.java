package ru.edu.service.command.constants;

public class Constants {
    public final static String DATE_PATTERN = "yyyy-MM-dd";
    public final static String NO_TELEGRAM_ACCOUNT_MESSAGE = "Ваш телеграм аккаунт не подвязан к профилю.";
    public final static String HELP_MESSAGE = "❗*Список команд*\n" +
            "/reg - для привязки telegram account пользователю\n" +
            "/task - для получения событий на сегодня\n" +
            "/task yyyy-mm-dd - для получения событий за определенный день\n" +
            "/add - для создания новой задачи\n" +
            "/cancel НОМЕР ЗАДАЧИ yyyy-mm-dd - для удаления задачи из списка событий за определенный день\n" +
            "/finish НОМЕР ЗАДАЧИ - для отметки, что задача выполнена";
}
