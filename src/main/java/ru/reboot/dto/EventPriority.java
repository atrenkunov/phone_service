package ru.reboot.dto;

public enum EventPriority {
    HIGH,
    MEDIUM,
    LOW
}
