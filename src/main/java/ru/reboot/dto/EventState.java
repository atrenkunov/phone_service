package ru.reboot.dto;

public enum EventState {
    TODO,
    CANCELED,
    DONE
}
